# primo-explore-eth-connectedpapers

## Description

This module integrates links to Connected Papers in Primo VE if there is a DOI.
The links are already visible in the result list.
See for API: https://connectedpapers.notion.site/Connected-Papers-APIs-6f67ca11cf834b42bc637f3357c7274d.

### Screenshot

#### Connected Papers links
![screenshot](https://gitlab.com/ethlibrary/primo-explore-modules/primo-explore-eth-connectedpapers/-/raw/master/screenshot1.jpg)

## Installation

1. Assuming you've installed and are using [primo-explore-devenv](https://github.com/ExLibrisGroup/primo-explore-devenv).

2. Navigate to your view root directory. For example:
    ```
    cd primo-explore/custom/MY_VIEW_ID
    ```
3. If you do not already have a package.json file in this directory, create one:
    ```
    npm init -y
    ```
4. Install this package:
    ```
    npm install primo-explore-eth-connectedpapers --save
    ```



## Usage

Once installed, inject `ethConnectedPapersModule` as a dependency, and then add the eth-connected-papers-component directive to the prmSearchResultAvailabilityLineAfter component.
You can do this in the js/main.js.

```js

import 'primo-explore-eth-connectedpapers';

var app = angular.module('viewCustom', ['ethConnectedPapersModule']);

app.component('prmSearchResultAvailabilityLineAfter',  {
        bindings: {parentCtrl: '<'},
        template: `<eth-connected-papers-component after-ctrl="$ctrl"></eth-connected-papers-component>`
    })

```
